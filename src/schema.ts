import {
  GraphQLSchema,
  GraphQLObjectType,
  GraphQLString,
  GraphQLNonNull,
  GraphQLList,
} from "graphql";

export default new GraphQLSchema({
  query: new GraphQLObjectType({
    name: "Query",
    fields: {
      hello: {
        type: new GraphQLNonNull(GraphQLString),
        description: "Welcome",
        args: {
          names: {
            type: new GraphQLList(new GraphQLNonNull(GraphQLString)),
            defaultValue: ["World"],
          },
        },
        resolve: (source, args, context, info) => {
          return `Hello ${args.names.join(", ")}!`;
        },
      },
    },
  }),
});
